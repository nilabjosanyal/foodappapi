from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.generics import UpdateAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User
from API.models import ImageData, Profile, FoodCategory, Comments, LikedRestaurants, UpVoteData, DownVoteData
from .serializers import (UserSerializer, ImageDataSerializer, ProfileSerializer, CategorySerializer, CommentSerializer, LikedRestaurantsSerializer, UsernameSerializer, UpVoteDataSerializer, DownVoteDataSerializer, NewPostSerializer)
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from django.core import serializers
from rest_framework.parsers import FileUploadParser

#for user login and logout
from django.contrib.auth import login, logout


#APIViews

class UserList(APIView):

	def get(self, request, username, format=None):
		users = User.objects.get(username=username)
		serializer = UserSerializer(users)
		return Response(serializer.data)

class AuthView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = {
            'user': unicode(request.user),  # `django.contrib.auth.User` instance.
            'auth': unicode(request.auth),  # None
        }
        return Response(content)

class TokenUserDetail(APIView):
    def get(self, request, format=None):
        key = request.META['HTTP_KEY']
        user_obj = Token.objects.get(key=key)
        serializer = UserSerializer(user_obj.user)
        return Response(serializer.data)

class UserDetailById(APIView):
    def get(self, request, id, format=None):
        user_obj = User.objects.get(id=id)
        serializer = UserSerializer(user_obj)
        s2 = UsernameSerializer(user_obj)
        data = []
        data.append(serializer.data)
        data.append(s2.data)
        return Response(data)

class ImageDataView(APIView):

	def get(self, request):
		img_data = ImageData.objects.all().order_by('id').reverse()
		serializer = ImageDataSerializer(img_data, many=True)
		return Response(serializer.data)

class ImageDetail(APIView):

	def get(self, request, pk, format=None):
		img_data = ImageData.objects.get(id=pk)
		serializer = ImageDataSerializer(img_data)
		return Response(serializer.data)

class PutUserProfile(UpdateAPIView):
    serializer_class = ProfileSerializer

    def update(self, request, *args, **kwargs):
        instance = Profile.objects.get(user__username=self.kwargs['username'])
        print instance.user.username
        print request.data.get('user_type')
        instance.user_type = request.data.get('user_type')
        instance.save(update_fields=['user_type'])

        return Response(serializer.data)

class CategoryList(APIView):
    def get(self, request):
        cat_obj = FoodCategory.objects.all()
        serializer = CategorySerializer(cat_obj, many=True)
        return Response(serializer.data)

class CategoryFilter(APIView):
    def get(self, request, cat_name, format=None):
        obj = ImageData.objects.filter(category__category_name=cat_name)
        serializer = ImageDataSerializer(obj, many=True)
        return Response(serializer.data)

class ListComments(APIView):
    queryset = Comments.objects.all()

    def get(self, request, img_pk, format=None):
        obj = Comments.objects.filter(image_data__id=img_pk)
        serializer = CommentSerializer(obj, many=True)
        username = []
        data = []
        i = 0

        for item in serializer.data:
            print item['user']
            username.append(User.objects.get(id=item['user']).username)
            i = i + 1

        print username
        #print serializer.data[1]['user']
        data.append(serializer.data)
        data.append(username)
        return Response(data)

    def post(self, request, img_pk, *args, **kwargs):
        serializer = CommentSerializer(data=request.data)
        print request.data
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserLikes(APIView):
    def get(self, request, username, format=None):
        obj = LikedRestaurants.objects.filter(user__username=username)
        serializer = LikedRestaurantsSerializer(obj, many=True)
        return Response(serializer.data)

class UpVoteView(APIView):
    def post(self, request, *args, **kwargs):
        print 'post upvote'
        print request.data['image_data']

        image_id = request.data['image_data']
        user_id = request.data['user']



        try:
            UpVoteData.objects.get(user__id=user_id, image_data=image_id)
            return Response({"error":"You already voted!"})
        except UpVoteData.DoesNotExist:
            serializer = UpVoteDataSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DownVoteView(APIView):
    def post(self, request, *args, **kwargs):
        user_id = request.data['user']
        image_id = request.data['image_data']
        try:
            DownVoteData.objects.get(user__id=user_id, image_data=image_id)
            return Response({"error":"You already voted!"})
        except DownVoteData.DoesNotExist:
            serializer = DownVoteDataSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class NewPost(APIView):

    def get(self, request, format=None):
		img_data = ImageData.objects.all()
		serializer = NewPostSerializer(img_data, many=True)
		return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = NewPostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserPosts(APIView):
    def get(self, request, id, format=None):
        img_obj = ImageData.objects.filter(user__id=id)
        serializer = ImageDataSerializer(img_obj, many=True)
        return Response(serializer.data)

class UserLikedPosts(APIView):
    def get(self, request, id, format=None):
        like_obj = LikedRestaurants.objects.filter(user__id=id)
        serializer = LikedRestaurantsSerializer(like_obj, many=True)
        return Response(serializer.data)
#End of APIView
