# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-22 10:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0009_restaurant'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imagedata',
            name='consumption_type',
            field=models.CharField(choices=[('Breakfast', 'Breakfast'), ('Lunch', 'Lunch'), ('Dinner', 'Dinner'), ('Snack', 'Snack'), ('Munch', 'Munch')], max_length=128),
        ),
    ]
