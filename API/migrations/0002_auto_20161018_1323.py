# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-18 13:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='foodcategory',
            name='category_name',
            field=models.CharField(default='Junk', max_length=64),
        ),
    ]
