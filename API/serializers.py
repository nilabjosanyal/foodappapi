from rest_framework import serializers
from API.models import Profile, ImageData, FoodCategory, Restaurant, Comments, LikedRestaurants, UpVoteData, DownVoteData
from django.contrib.auth.models import User


class ProfileSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ('__all__')

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodCategory
        fields = ('id','category_name',)

class UsernameSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=200)

class UserSerializer(serializers.ModelSerializer):
	profile = ProfileSerializer(read_only=True)


	class Meta:
		model = User
		fields = ('id', 'username', 'password', 'email', 'profile',)

class ImageDataSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    category = CategorySerializer(read_only=True)

    class Meta:
        model = ImageData
        fields = ('__all__')

class Restaurant(serializers.ModelSerializer):

    class Meta:
        model = Restaurant
        fields = ('__all__')

class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comments
        fields = ('__all__')

class LikedRestaurantsSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    restaurant = Restaurant(read_only=True)

    class Meta:
        model = LikedRestaurants
        fields = ('__all__')

class UpVoteDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = UpVoteData
        fields = ('__all__')

class DownVoteDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = DownVoteData
        fields = ('__all__')

class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension

class NewPostSerializer(serializers.ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )
    class Meta:
        model = ImageData
        fields = ('__all__')
