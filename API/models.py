from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

# Create your models here.

class Profile(models.Model):
	CAT_USER = (
	    ('CF', 'Chef'),
	    ('NT', 'Nutritionist'),
	    ('ET', 'Enthusiast'),
	)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	active_promo_codes = models.CharField(max_length=10000, blank=True)
	liked_restaurants = models.CharField(max_length=10000, blank=True)
	user_type = models.CharField(max_length=128, choices=CAT_USER)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class FoodCategory(models.Model):
    category_name = models.CharField(max_length=64, default="Junk")

    def __str__(self):
        return self.category_name

class ImageData(models.Model):
    CONSUMPTION_TYPE_CHOICE = (
        ('Breakfast', 'Breakfast'),
        ('Lunch', 'Lunch'),
        ('Dinner', 'Dinner'),
        ('Snack', 'Snack'),
        ('Munch', 'Munch'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey(FoodCategory, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='')
    dish_name = models.CharField(max_length=32)
    image_title = models.CharField(max_length=32)
    description = models.CharField(max_length=100)
    restuarant_name = models.CharField(max_length=50)
    suggested_recipes = models.CharField(max_length=1000)
    ingredients = models.CharField(max_length=1000)
    prep_time = models.IntegerField("In minutes")
    seperate_items = models.CharField(max_length=10000)
    item_calorie_count = models.DecimalField(max_digits=5, decimal_places=2)
    dish_calorie_count = models.DecimalField(max_digits=5, decimal_places=2)
    nutritional_info = models.CharField(max_length=128)
    consumption_type = models.CharField(max_length=128, choices=CONSUMPTION_TYPE_CHOICE)
    portion_had = models.CharField(max_length=32)
    upvote = models.IntegerField(default=0)
    downvote = models.IntegerField(default=0)

    def __str__(self):
        return self.dish_name


class UpVoteData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image_data = models.ForeignKey(ImageData, on_delete=models.CASCADE)

class DownVoteData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image_data = models.ForeignKey(ImageData, on_delete=models.CASCADE)

@receiver(pre_save, sender=UpVoteData)
def check_upvote(sender, instance, **kwargs):
    try:
        DownVoteData.objects.filter(image_data=instance.image_data, user=instance.user)
    except DownVoteData.DoesNotExist:
        pass
    else:
        DownVoteData.objects.filter(image_data=instance.image_data, user=instance.user).delete()
        print "Prev DownVoteData %d" % instance.image_data.downvote
        if instance.image_data.downvote != 0:
            instance.image_data.downvote -= 1
        print "Current DownVoteData %d" % instance.image_data.downvote
        instance.image_data.save()

@receiver(post_save, sender=UpVoteData)
def count_upvote(sender, instance, **kwargs):
    print "Prev UpVote = %d" % instance.image_data.upvote;
    instance.image_data.upvote += 1
    instance.image_data.save()
    print "Current UpVote = %d" % instance.image_data.upvote;

@receiver(pre_save, sender=DownVoteData)
def check_downvote(sender, instance, **kwargs):
    try:
        UpVoteData.objects.filter(image_data=instance.image_data, user=instance.user)
    except DownVoteData.DoesNotExist:
        pass
    else:
        UpVoteData.objects.filter(image_data=instance.image_data, user=instance.user).delete()
        print "Prev UpVoteData %d" % instance.image_data.upvote
        if instance.image_data.upvote != 0:
            instance.image_data.upvote -= 1
        print "Current UpVoteData %d" % instance.image_data.upvote
        instance.image_data.save()

@receiver(post_save, sender=DownVoteData)
def count_downvote(sender, instance, **kwargs):
    print "Prev DownVote = %d" % instance.image_data.downvote;
    instance.image_data.downvote += 1
    instance.image_data.save()
    print "Current DownVote = %d" % instance.image_data.downvote;

class Comments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image_data = models.ForeignKey(ImageData, on_delete=models.CASCADE)
    comment = models.TextField()

class Promocode(models.Model):
    code = models.CharField(max_length=10)
    applicable_at = models.CharField(max_length=1000)
    locations = models.CharField(max_length=1000)

class Restaurant(models.Model):
    name = models.CharField(max_length=512)
    location = models.CharField(max_length=64)
    best_dishes = models.OneToOneField(ImageData, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class LikedRestaurants(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
