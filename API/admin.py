from django.contrib import admin
from API.models import Profile, ImageData, FoodCategory, UpVoteData, DownVoteData, Comments, Restaurant, LikedRestaurants

# Register your models here.
admin.site.register(Profile)
admin.site.register(ImageData)
admin.site.register(FoodCategory)
admin.site.register(UpVoteData)
admin.site.register(Comments)
admin.site.register(DownVoteData)
admin.site.register(Restaurant)
admin.site.register(LikedRestaurants)
