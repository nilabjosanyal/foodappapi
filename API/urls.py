from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from API import views

urlpatterns = [
	url(r'^users/(?P<username>\w+)$', views.UserList.as_view()),
    url(r'^current_user_detail/$', views.TokenUserDetail.as_view()),
    url(r'^user_detail/(?P<id>[0-9]+)$', views.UserDetailById.as_view()),
	url(r'^auth/$', views.AuthView.as_view()),
	url(r'^images/$', views.ImageDataView.as_view()),
	url(r'^images/(?P<pk>[0-9]+)$', views.ImageDetail.as_view()),
    url(r'^update_profile/(?P<username>\w+)$', views.PutUserProfile.as_view()),
    url(r'^likes/(?P<username>\w+)$', views.UserLikes.as_view()),
    url(r'^categories/$', views.CategoryList.as_view()),
    url(r'^categories/(?P<cat_name>\w+)$', views.CategoryFilter.as_view()),
    url(r'^comments/(?P<img_pk>[0-9]+)$', views.ListComments.as_view()),
    url(r'^upvote/', views.UpVoteView.as_view()),
    url(r'^downvote/', views.DownVoteView.as_view()),
    url(r'^new_post/', views.NewPost.as_view()),
    url(r'^user_post/(?P<id>[0-9]+)$', views.UserPosts.as_view()),
    url(r'^liked_restaurants/(?P<id>[0-9]+)$', views.UserLikedPosts.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
